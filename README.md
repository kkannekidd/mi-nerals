# (Mi)nerals #

Small-scale app that demonstrates how PHP can interact with a MySQL database with the help of Ajax and JQuery.

* Last modified: November 23rd, 2016.
* URL: http://kiddography.com/projects/minerals-app/client.html
* Languages: HTML/CSS, Bootstrap, PHP, JavaScript, JQuery, AJAX, MySQL

## **SOURCES** ##

### HTML/CSS: ###
* [Bootstrap](http://getbootstrap.com/) - 3.3.7
* [Fixed Top Navbar Example for Bootstrap](https://getbootstrap.com/examples/navbar-fixed-top/) - Bootstrap
* [Font Awesome Icons](http://fontawesome.io/icons/) - 4.6.3 © Dave Gandy
* [Glyphicons](http://glyphicons.com/) - © Jan Kovarik
* [Default Sizes for Twitter Bootstrap’s Media Queries](https://scotch.io/tutorials/default-sizes-for-twitter-bootstraps-media-queries) - © Mar 04, 2014, Nicholas Cerminara (Scotch.io).
* [Device Media Queries](http://resizr.co/) - © Piers Rueb
* [HTML Entities Encoder / Decoder ](http://www.web2generators.com/html-based-tools/online-html-entities-encoder-and-decoder) - © IBE Software (2016)


### JavaScript/JQuery: ###
* [JQuery](https://jquery.com/) - 2.2.2
* [JQuery Easing Plugin](http://gsgd.co.uk/sandbox/jquery/easing/) - v1.3
* [JQuery UI](https://jqueryui.com/) - 1.10.4
* [jQuery Waypoints](https://jqueryui.com/) - 2.0.3 - © 2011-2013 Caleb Troughton
* [Counter-Up](https://github.com/bfintal/Counter-Up) - © Benjamin Intal
* [Collapsible Accordion](http://www.w3schools.com/howto/howto_js_accordion.asp) - w3schools.com - © w3schools.com.



## **PENDING** ##
* ~~Add "Code" section.~~
* ~~Make responsive.~~
* ~~Clean up scripts.~~
* ~~Add entries to database.~~
* ~~Convert in-line styles to external.~~
* ~~Streamline id and class names in CSS.~~
* ~~Add "How are they classified?" section.~~
* ~~Add "Physical properties" section.~~
